# Om mani padme hum

- Oṃ maṇi padme hūṃ[1] (Sanskrit: ओं मणिपद्मे हूं, IPA: [õːː məɳipəd̪meː ɦũː]) is the six-syllabled Sanskrit mantraparticularly associated with the four-armed Shadakshari form of Avalokiteshvara, the bodhisattva of compassion. The first word Om is a sacred syllable found in Indian religions. The word Mani means "jewel" or "bead", Padme meaning the "lotus flower", the Buddhist sacred flower, while Hum represents the spirit of enlightenment.
- It is commonly carved onto rocks or written on paper which is inserted into prayer wheels. When an individual spins the wheel, it is said that the effect is the same as reciting the mantra as many times as it is duplicated within the wheel.

## Lesson 1 - Collecting Karma Points 
So since repeating this mantra over and over is as powerful as doing the mediation and mantra, lets build a simple function to repeat it as much as we want.

#### OM MANI PADME HUM 


We need a basic function to repeat a given amount of times. Spice it up if you like.



